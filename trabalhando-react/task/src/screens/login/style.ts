import { StyleSheet} from 'react-native'
import styled from 'styled-components/native'

export const Container = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    background-color: #181419;
    font-family: sans-serif;

`;

export const BoxTitle = styled.View`
    align-items: center;
    justify-content: center;
    bottom: 85px;
    margin-top: 90px;
    color: #fff;
    font-size: 25px;
   
`;

export const TextTitle = styled.Text`
    color: #fff;
    font-size: 25px;
    font-weight: semibold;
    margin: 15px;
    margin-bottom: 40px;
`;

export const SubTitle = styled.Text`
    color: #fff;
    font-size: 25px;
    margin: 15px;

`;


export const Input = styled.TextInput`
    color: #B6AAAA;
    padding: 10px;
    background-color: #D9D9D9;
    border-radius: 5;
    width: 298px;
    height: 44px;
    margin: 12px;
    outline-style: none;
`;  

 export const Image = styled.Image`
     width: 81px;
     height: 75px;
     background-color: #fff;
     border-radius: 5px;

 `;

export const Button = styled.TouchableOpacity`
    background-color: #6C63FF;
    width: 172px;
    height: 50px;
    border-radius: 5px;
    margin: 20px;
    cursor: pointer;
    color: #fff;
    font-weight: semibold;
    font-size: 20px;
    text-align: center;
    margin: 12px;
    border: #6C63FF;
    justify-content: center;
    align-items: center;

`;

export const BtnText = styled.Text`
    color: #6C63FF;
    justify-content: center;
    align-items: center;

`;


export const Senha = styled.Text`
    color: #fff;
    font-size: 14px;
    margin: 10px;
    
`;

export const TextUnderButton = styled.Text`
    color: #fff;
    font-size: 14px;
    margin: 8px;
  
`;

export const SubTextUnderButton = styled.Text`
    color: #6C63FF;
    font-size: 14px;
    margin: 8px;
`;

