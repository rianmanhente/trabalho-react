

import { useNavigation } from "@react-navigation/native"
import { Container , BoxTitle , TextTitle , SubTitle , Input, Button , Senha, TextUnderButton , BtnText, SubTextUnderButton} from "./style"



export function Login() {
    const msg1 = () => {
        alert('Uma nova senha foi enviada para seu email!')
    }

    const log= () => {
        alert('Bem-vindo de volta! Aproveite as promos no precinho da Reus!')
    }
    
    const navigation = useNavigation()
    return(
        <Container>
    
            <BoxTitle>REUS-E</BoxTitle>
            <TextTitle>Login!</TextTitle>
            <SubTitle>Faça login e encontre</SubTitle>
            <SubTitle>uma peça perfeita para seu pc!</SubTitle>
            <Input placeholder ='email'></Input>
            <Input placeholder ='senha'></Input>
            <Senha>Esqueceu sua senha?</Senha>
            <BtnText onPress={(msg1)}>Clique aqui</BtnText>
            <Button onPress={(log)}>Login</Button>
            <TextUnderButton>Não tem conta?</TextUnderButton>
            <SubTextUnderButton  onPress={() => navigation.navigate('Cadastro')}>Cadastre-se</SubTextUnderButton>
            {/* <Image source={require("./img/undraw.png")}></Image> */}

            

        </Container>
    )
}

