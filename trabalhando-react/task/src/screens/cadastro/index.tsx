

import { Container , BoxTitle , TextTitle , SubTitle , Input, Image, Button , TextUnderInput} from "./styles"

import { useNavigation} from '@react-navigation/native'



export function Cadastro() {

    const msg1 = () => {
        alert('Parábens! Você se tornou um membro da Reus!')
    }

    const navigation = useNavigation()
    return(
        <Container>
    
            <BoxTitle>REUS-E</BoxTitle>
            <TextTitle>Cadastre-se</TextTitle>
            <SubTitle>Crie seu cadastro</SubTitle>
            <SubTitle>Aproveite as promoçoẽs!</SubTitle>
            <Input placeholder='nome'></Input>
            <Input placeholder ='email'></Input>
            <Input placeholder ='senha'></Input>
            <TextUnderInput onPress={() => navigation.navigate('Login')}>Já é membro? Faça login!</TextUnderInput>
            <Button onPress={(msg1)}>Cadastre-se</Button>
            <Image source={require("./img/undraw.png")}></Image>

            

        </Container>
    )
}


