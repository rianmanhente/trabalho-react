import * as React from 'react'
import { NavigationContainer} from "@react-navigation/native"   
import { createNativeStackNavigator } from '@react-navigation/native-stack'

const Stack = createNativeStackNavigator()

import { Cadastro } from '../cadastro'
import {Login} from '../login'

export function AppRoutes(){
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Cadastro">
                <Stack.Screen name="Cadastro" component={Cadastro} />
                <Stack.Screen name="Login" component={Login} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}